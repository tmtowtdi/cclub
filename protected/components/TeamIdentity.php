<?php
class TeamIdentity extends CUserIdentity
{
    private $_id;
    private $_name;
    public $email;
    public $password;

    public function __construct($email,$password)
    {
        $this->email = $email;
        $this->password = $password;
    }
	/**
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $user = YarismaTeams::model()->findByAttributes(array('email'=>$this->email));
		if($user ===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if($user->password != YarismaTeams::hashPassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
        {
            $this->_id = $user->id;
            $this->_name = $user->group_name;

            $this->setState('email',$user->email);
			$this->errorCode=self::ERROR_NONE;
        }
		return !$this->errorCode;
	}

    public function getName()
    {
        return $this->_name;
    }

    public function getId()
    {
        return $this->_id;
    }
}

<?php
class UserIdentity extends CUserIdentity
{
    private $_id;
    public $email;
    public $password;

    public function __construct($email,$password)
    {
        $this->email = $email;
        $this->password = $password;
    }
	/**
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $user = User::model()->findByAttributes(array('email'=>$this->email));
		if($user ===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if($user->password != User::hashPassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
        {
            $this->_id = $user->id;

            $this->setState('email',$user->email);
			$this->errorCode=self::ERROR_NONE;
        }
		return !$this->errorCode;
	}

    public function getId()
    {
        return $this->_id;
    }
}

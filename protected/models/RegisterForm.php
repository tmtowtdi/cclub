<?php
class RegisterForm extends CFormModel
{
    public $verifyCode;
    public $password;
    public $password_repeat;
    public $name;
    public $lname;
    public $email;
    function rules(){
        return array(
			array('email, password, password_repeat, name, lname', 'required'),
            array('email', 'email'),
			array('email, password, name, lname', 'length', 'max'=>255),
            array('password','compare'),
            array('verifyCode', 'captcha', 'allowEmpty'=> !CCaptcha::checkRequirements()),
        );
    }
}

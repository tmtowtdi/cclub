<?php
class TeamRegisterForm extends CFormModel
{
    public $email;
    public $group_name;
    public $password;
    public $password_repeat;
    public $verifyCode;
    public $universite;
    public $katilimcilar;
    public $tel;
    public $adres;

    function rules(){
        return array(
			array('group_name, email, password, password_repeat, universite, katilimcilar, tel, adres', 'required'),
            array('email', 'email'),
            array('tel', 'numerical'),
			array('group_name, email, password, universite, katilimcilar, tel, adres', 'length', 'max'=>255),
            array('password','compare'),
            array('verifyCode', 'captcha', 'allowEmpty'=> !CCaptcha::checkRequirements()),
        );
    }
}

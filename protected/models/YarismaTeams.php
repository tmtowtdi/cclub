<?php

/**
 * This is the model class for table "yarisma_teams".
 *
 * The followings are the available columns in table 'yarisma_teams':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $universite
 * @property string $katilimcilar
 * @property string $tel
 * @property string $adres
 * @property string $group_name
 */
class YarismaTeams extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return YarismaTeams the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'yarisma_teams';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_name, email, password, universite, katilimcilar, tel, adres', 'required'),
			array('group_name, email, password, universite, katilimcilar, tel, adres', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('group_name, id, email, password, universite, katilimcilar, tel, adres', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'universite' => 'Universite',
			'katilimcilar' => 'Katilimcilar',
			'tel' => 'Tel',
			'adres' => 'Adres',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('universite',$this->universite,true);
		$criteria->compare('katilimcilar',$this->katilimcilar,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('adres',$this->adres,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterValidate()
    {
        $this->password = self::hashPassword($this->password);
        return parent::afterValidate();
    }

    public static function hashPassword($pass)
    {
        return md5($pass);
    }
}

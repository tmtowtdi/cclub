<?php

/**
 * This is the model class for table "cclub_activities".
 *
 * The followings are the available columns in table 'cclub_activities':
 * @property integer $id
 * @property string $title
 * @property integer $date
 * @property string $short
 * @property string $content
 * @property string $poster
 */
class Activity extends CActiveRecord
{
    private $_fpath;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Activity the static model class
	 */

    public function __construct($s='insert')
    {
        $this->_fpath = getcwd().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'afis'.DIRECTORY_SEPARATOR;
        parent::__construct($s);
    }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cclub_activities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('poster', 'file', 'types'=>'jpg, jpeg, png, gif'),
			array('title, date, short, content, poster', 'required'),
			array('date', 'numerical', 'integerOnly'=>true),
			array('title, short', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, date, short, content, poster', 'safe', 'on'=>'search'),
		);
	}

    public function getPosterLink()
    {
        return Yii::app()->request->baseUrl . "/images/afis/".$this->poster;
    }

    public function beforeDelete()
    {
        $path = $this->_fpath.$this->poster;

        if(file_exists($path))
            unlink($path);
        return parent::beforeDelete();
    }

    public function getFpath()
    {
        return $this->_fpath;
    }

    public function savePoster()
    {
        if(!file_exists($this->_fpath)) mkdir($this->_fpath);
        $name = $this->_fpath.$this->id.".".$this->poster->getExtensionName();
        $this->poster->saveAs($name);
        $afis = Yii::app()->image->load($name);
        $afis->resize(273,0,Image::WIDTH);
        $afis->save();
        $this->poster = $name;
        return $this->save(false);
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'date' => 'Date',
			'short' => 'Short',
			'content' => 'Content',
			'poster' => 'Poster',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('date',$this->date);
		$criteria->compare('short',$this->short,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('poster',$this->poster,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

<?php

/**
 * This is the model class for table "question_result".
 *
 * The followings are the available columns in table 'question_result':
 * @property integer $id
 * @property integer $team_id
 * @property integer $question_id
 * @property integer $input_id
 * @property integer $result
 * @property integer $request_time
 */
class QuestionResult extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return QuestionResult the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'question_result';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('team_id, question_id, input_id, result, request_time', 'required'),
			array('team_id, question_id, input_id, result, request_time', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, team_id, question_id, input_id, result, request_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'team_id' => 'Team',
			'question_id' => 'Question',
			'input_id' => 'Input',
			'result' => 'Result',
			'request_time' => 'Request Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('team_id',$this->team_id);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('input_id',$this->input_id);
		$criteria->compare('result',$this->result);
		$criteria->compare('request_time',$this->request_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
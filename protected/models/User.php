<?php

/**
 * This is the model class for table "cclub_users".
 *
 * The followings are the available columns in table 'cclub_users':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $lname
 * @property integer $role
 */
class User extends CActiveRecord
{
        public $password_repeat;
        public $verifyCode;
	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cclub_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('email','unique'),
			array('email, password, password_repeat, name, lname, verifyCode', 'required'),
            array('email', 'email'),
			array('email, password, name, lname', 'length', 'max'=>255),
            array('password','compare'),
            array('verifyCode', 'captcha', 'allowEmpty'=> !CCaptcha::checkRequirements()),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('email, password, name, lname, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'profilePosts' => array(self::HAS_MANY, 'Post','profile_id'),
		);
	}

    public function getFullName()
    {
        return $this->name . " " . $this->lname;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'name' => 'Name',
			'lname' => 'Last name',
			'role' => 'Role',
		);
	}

    public function afterValidate()
    {
        $this->password = self::hashPassword($this->password);
        return parent::afterValidate();
    }

    public function beforeSave()
    {
        if( $this->isNewRecord )
            $this->role = 1;
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('role',$this->role);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getRoleName()
    {
        $roles = self::roleNames();
        return $roles[$this->role];
    }

    public static function roleNames()
    {
        return array(
            0 => "NotActivated",
            1 => "New",
            2 => "Official",
            3 => "CgHead",
            4 => "YK",
            5 => "Admin",
        );
    }

    public static function hashPassword($pass)
    {
        return md5($pass);
    }
}

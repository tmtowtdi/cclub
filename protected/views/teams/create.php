<?php
$this->breadcrumbs=array(
	'Yarisma Teams'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List YarismaTeams', 'url'=>array('index')),
	array('label'=>'Manage YarismaTeams', 'url'=>array('admin')),
);
?>

<h1>Create YarismaTeams</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Yarisma Teams'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List YarismaTeams', 'url'=>array('index')),
	array('label'=>'Create YarismaTeams', 'url'=>array('create')),
	array('label'=>'View YarismaTeams', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage YarismaTeams', 'url'=>array('admin')),
);
?>

<h1>Update YarismaTeams <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Yarisma Teams',
);

$this->menu=array(
	array('label'=>'Create YarismaTeams', 'url'=>array('create')),
	array('label'=>'Manage YarismaTeams', 'url'=>array('admin')),
);
?>

<h1>Yarisma Teams</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
$this->breadcrumbs=array(
	'Yarisma Teams'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List YarismaTeams', 'url'=>array('index')),
	array('label'=>'Create YarismaTeams', 'url'=>array('create')),
	array('label'=>'Update YarismaTeams', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete YarismaTeams', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage YarismaTeams', 'url'=>array('admin')),
);
?>

<h1>View YarismaTeams #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'email',
		'password',
		'universite',
		'katilimcilar',
		'tel',
		'adres',
	),
)); ?>

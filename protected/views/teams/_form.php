<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'yarisma-teams-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'universite'); ?>
		<?php echo $form->textField($model,'universite',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'universite'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'katilimcilar'); ?>
		<?php echo $form->textField($model,'katilimcilar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'katilimcilar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tel'); ?>
		<?php echo $form->textField($model,'tel',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adres'); ?>
		<?php echo $form->textField($model,'adres',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'adres'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

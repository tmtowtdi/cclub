<?php
$this->breadcrumbs=array(
	'Activities'=>array('index'),
	$model->title,
);
$this->layout = '//layouts/column1';

// not working now.. because 1 columnt thing
$this->menu=array(
    array('label'=>'List Activity', 'url'=>array('index')),
    array('label'=>'Create Activity', 'url'=>array('create'),'visible'=>Yii::app()->user->checkAccess('YK')),
    array('label'=>'Update Activity', 'url'=>array('update', 'id'=>$model->id),'visible'=>Yii::app()->user->checkAccess('YK')),
	array('label'=>'Delete Activity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->checkAccess('Admin')),
	array('label'=>'Manage Activity', 'url'=>array('admin'),'visible'=>Yii::app()->user->checkAccess('Admin')),
);
?>
<div class="two-thirds">
<h1><?php echo $model->title; ?></h1>
<div class="hr"></div>

<?php
echo $model->content;
?>
<div class="hr"></div>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$media,
    'itemView'=>'_mediaView',   
    'summaryText' => '',
    'emptyText' => '',
));
if(Yii::app()->user->checkAccess('YK')){
    $media =  new Media;
    $media->activity_id = $model->id;
    $this->renderPartial('_mediaForm',array('model'=>$media));
}
?>

</div>
<div class="one-third">
<?php
echo CHtml::image($model->posterLink);
?></div>

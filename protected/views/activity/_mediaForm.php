
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'activity-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array( 'enctype' => 'multipart/form-data',),
    )
); ?>

<?php echo $form->hiddenField($model,'activity_id'); ?>

	<div class="row">
        <?php echo $form->labelEx($model,'file'); ?>
        <?php echo $form->fileField($model,'file'); ?>
		<?php echo $form->error($model,'file'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textField($model,'text',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Add'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

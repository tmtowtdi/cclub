<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'activity-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array( 'enctype' => 'multipart/form-data',),
    )
); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'poster'); ?>
		<?php echo $form->fileField($model,'poster'); ?>
		<?php echo $form->error($model,'poster'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title', array('class' => 'overlabel')); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker',
        array(
            'name'=>'Activity[date]',
            'flat' => true, // tells the widget to show the calendar inline
        )
    );
    ?>

	<div class="row">
		<?php echo $form->labelEx($model,'short', array('class' => 'overlabel')); ?>
		<?php echo $form->textField($model,'short'); ?>
		<?php echo $form->error($model,'short'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content', array('class' => 'overlabel')); ?>
		<?php echo $form->textArea($model,'content'); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

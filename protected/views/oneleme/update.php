<?php
$this->breadcrumbs=array(
	'Yarisma Questions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List YarismaQuestion', 'url'=>array('index')),
	array('label'=>'Create YarismaQuestion', 'url'=>array('create')),
	array('label'=>'View YarismaQuestion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage YarismaQuestion', 'url'=>array('admin')),
);
?>

<h1>Update YarismaQuestion <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
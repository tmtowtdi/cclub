<?php
$this->breadcrumbs=array(
	'Yarisma Questions',
);
?>

<h1>Yarisma Sorulari</h1>
<p>Üye olduktan sonra her sorunun altında bir input dosyası indirebilirsiniz. İndirdiğiniz dosyaya ait sonucu yüklemek için 15 dakika zamanınız olacak.
Bu süre içinde doğru cevabı yüklemezseniz bu soru için şansınızı kaybedeceksiniz. Bu yüzden yükleme isteği yapmadan önce programınızın doğru çalıştığından emin olun. </p>
<ul>
<?php 
foreach ($dataProvider as $que) {
	echo "<li>";
	echo CHtml::link($que->name, array('/oneleme/show', 'id' => $que->id));
	if(array_key_exists($que->id, $results) && $results[$que->id]->result > 0)
	{
		echo "[OK]";
	}
	echo "</li>";
}
?>
</ul>
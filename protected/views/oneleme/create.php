<?php
$this->breadcrumbs=array(
	'Yarisma Questions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List YarismaQuestion', 'url'=>array('index')),
	array('label'=>'Manage YarismaQuestion', 'url'=>array('admin')),
);
?>

<h1>Create YarismaQuestion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
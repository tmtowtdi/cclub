<h1><?php echo $model->name; ?></h1>

<?php echo $model->question; ?>


<div>
	<br>
	<br>
</div>
<?php if ($qmodel && $qmodel->result > 0): ?>
	Bu soruyu doğru çözdünüz
<?php elseif ($timePassed): ?>
	<div id="TimePassed">
	Yüklemek için size ayrılan sürenin sonuna geldiniz...
	</div>
<?php else: ?>
	<p><?php echo CHtml::link("İnput indir", array("oneleme/requestInput", 'id' => $model->id)); ?></p>

	<?php if ($qmodel): ?>
		<script type="text/javascript">
			var time = new Date(<?php echo $qmodel->request_time; ?>*1000); 
			var timeToPass = 5 * 60 * 1000;

			console.log(timeToPass);
			$(function()
			{
				var timeRemained;
				function updateTimer() 
				{
					var now = new Date();
					var timeR = (timeToPass - (now - time));
					console.log(timeR);

					timeRemained = timeR > 0 ? new Date(timeR) : new Date(0);
					console.log(timeRemained);
					$('#TimeRemained').html(timeRemained.getMinutes() + ":" + timeRemained.getSeconds());
				}
				setInterval(updateTimer, 1000);
			});
		</script>
		<p><span id="TimeRemained"></span> dakika sonra bu soruya çözüm yükleme hakkını kaybedeceksin</p>

	<?php else: ?>
		İlk defa input indirdikten sonra 5 dk içerisinde output dosyanızı ve kaynak kodunuzu yüklemeniz gerekmektedir<br>
		Kaynak kodunuzu sonradan kopya kontrolü yapmak için istiyoruz.<br>
		Kaynak kodunuz aynı outputu vermezse sorunuz geçersiz sayılacaktır.	<br>
	<?php endif ?>
		Sonuçlar daha sonra kontrol edilecek <br>
<p style="color:#AD245B;">Tabi ki sistem herkes için mukemmel çalışamıyor.. kimsenin mağdur olmaması için cevapları kendimiz de kontrol ediyoruz.. 
	Tek yapmanız gereken soruları çözmek. Doğru cevap olduğundan eminseniz bize ulaşın veya bir sonraki kontrolü bekleyin..</p>
<div id="UploadForm" class="form">

	<?php $form=$this->beginWidget('CActiveForm', array
		(
		'id'=>'media-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=> array
		(
			'enctype'=>'multipart/form-data',
		),
	)); ?>

<p></p>
	<?php echo $form->errorSummary($umodel); ?>
		<?php echo $form->hiddenField($umodel,'questionId'); ?>
		<table>
		<tr>
		<td width="100">your output file:</td>
		<td><?php echo $form->fileField($umodel,'outputFile'); ?></td>
		<td><?php echo $form->error($umodel,'text'); ?></td>
		</tr>
		<tr>
		<td>your source file:</td>
		<td><?php echo $form->fileField($umodel,'sourceFile'); ?></td>
		<td><?php echo $form->error($umodel,'sourceFile'); ?></td>
		</tr>
		</table>

        <?php if(CCaptcha::checkRequirements()&& Yii::app()->user->isGuest):?>
        <p>
        <?php $this->widget('CCaptcha')?>
        </p>
        <p>
        <?php echo $form->labelEx($umodel, 'verifyCode',array('class'=>'overlabel'))?>
        <?php echo $form->textField($umodel, 'verifyCode',array('class'=>'textInput rounded'))?>
        <?php echo $form->error($umodel, 'verifyCode')?>
        </p>
        <?php endif?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
<?php endif ?>

<?php
$this->breadcrumbs=array(
	'Yarisma Questions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List YarismaQuestion', 'url'=>array('index')),
	array('label'=>'Create YarismaQuestion', 'url'=>array('create')),
	array('label'=>'Update YarismaQuestion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete YarismaQuestion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage YarismaQuestion', 'url'=>array('admin')),
);
?>

<h1>View YarismaQuestion #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'question',
	),
)); ?>

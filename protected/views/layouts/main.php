<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
	<!-- Style sheets -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fancybox.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/tooltip.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/default.css" />
	
	<!-- jQuery framework and utilities -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.easing.1.3.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/hoverIntent.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.bgiframe.min.js"></script>
	<!-- Drop down menus -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/superfish.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/supersubs.min.js"></script>
	<!-- Tooltips -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.cluetip.min.js"></script>
	<!-- Input labels -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.overlabel.min.js"></script>
	<!-- Anchor tag scrolling effects -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.localscroll-min.js"></script>
	<!-- Inline popups/modal windows -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fancybox-1.2.6.pack.js"></script>
	
	<!-- Font replacement (cufón) -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/cufon-yui.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/LiberationSans.font.js" type="text/javascript"></script>

	<!-- IE only includes (PNG Fix and other things for sucky browsers -->
	
	<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie-only.css">
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/pngFix.min.js"></script>
		<script type="text/javascript"> 
			$(document).ready(function(){ 
				$(document.body).supersleight();
			}); 
		</script> 
	<![endif]-->
	<!--[if IE]><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie-only-all-versions.css"><![endif]-->

	<!-- Functions to initialize after page load -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/onLoad.min.js"></script>

</head>
<body>
<!-- Site Container -->
<div id="Wrapper">
	<div id="PageWrapper">
		<div class="pageTop"></div>
		<div id="Header">
		
			<!-- Main Menu -->
			<div id="MenuWrapper">
				<div id="MainMenu">
					<div id="MmLeft"></div>
					<div id="MmBody">
                        <?php $this->widget('zii.widgets.CMenu',array(
                            'items'=>array(
                            	/*
                                array('label'=>'Anasayfa', 'url'=>array('/site/index')),
                                array('label'=>'Hakkımızda', 'url'=>'#', 'items' =>array(
                                    array('label'=>'Biz kimiz?', 'url'=>array('site/bizkimiz')),
                                    array('label'=>'Amaçlarımız', 'url'=>array('site/amaclar')),
                                    array('label'=>'Yönetim Kurulumuz', 'url'=>array('site/yonetim_kurulu')),
                                    array('label'=>'Aktif Üyelerimiz', 'url'=>array('site/aktif_uyeler')),
                                    array('label'=>'Bize Katılın', 'url'=>array('site/bize_katilin')),
                                )),
                                array('label'=>'Etkinlikler', 'url'=>array('/activity/index')),
                                */
                                array('label'=>'Yarışma', 'url'=>array('/yarisma/')),
                                array('label'=>'Ön Eleme Soruları', 'url'=>array('oneleme/')),
                                /*
                                array('label'=>'News', 'url'=>array('/news'),'visible'=>Yii::app()->user->checkAccess('YeniUye')),
                                array('label'=>'Çalışma Grupları', 'url'=>array('/group/index'), 'items' =>array(
                                    array('label'=>'Algoritma', 'url'=>'#'),
                                    array('label'=>'Dergi', 'url'=>'#'),
                                    array('label'=>'Mobil', 'url'=>'#'),
                                    array('label'=>'Oyun', 'url'=>'#'),
                                    array('label'=>'Web', 'url'=>'#'),
                                    array('label'=>'Yarışma', 'url'=>'#'),
                                )),
                                 */
                                array('label'=>'İletişim', 'url'=>array('/site/contact'),),
                            ),
                            'htmlOptions' => array('class' => 'sf-menu'),
                        )); ?>
                        <div class="mmDivider"></div>				
                        <?php $this->widget('zii.widgets.CMenu',array(
                            'encodeLabel' => false,
                            'items'=>array(
                                /*array('label'=>'<span class="mmLogin">Login</span>', 'url'=>array('/site/login'),'linkOptions'=> array('class'=>'login'),'visible'=> Yii::app()->user->isGuest),
                                array('label'=>'Register', 'url'=>array('/site/register'),'visible'=> Yii::app()->user->isGuest),
                                array('label'=>'<span class="mmLogin">Logout</span>', 'url'=>array('/site/logout'),'linkOptions'=> array('class'=>'logout'),'visible'=> !Yii::app()->user->isGuest),*/
                                array('label'=>'<span class="mmLogin">Takım Giriş</span>', 'url'=>array('/yarisma/login'),'visible'=> Yii::app()->team->isGuest),
                                array('label'=>'Çıkış('.Yii::app()->team->name.')', 'url'=>array('yarisma/logout'),'visible'=> !Yii::app()->team->isGuest),
                            ),
                            'htmlOptions' => array('id'=>'MmOtherLinks', 'class' => 'sf-menu'),
                        )); ?>
                    </div>
                    <div id="MmRight"></div>
                </div>
            </div>
			
			<!-- Search
			<div id="Search">
				<form action="#" id="SearchForm" method="post">
					<p style="margin:0;"><input type="text" name="SearchInput" id="SearchInput" value="" /></p>
					<p style="margin:0;"><input type="submit" name="SearchSubmit" id="SearchSubmit" class="noStyle" value="" /></p>
				</form>
			</div> -->
			
			<!-- Logo -->
			<div id="Logo">
			    <?php echo CHtml::link('','http://cclub.metu.edu.tr/');?>
			</div>
			
			<!-- End of Content -->
			<div class="clear"></div>
		
		</div>

		
		<div class="pageMain">
			<!-- Page Content -->
			<div class="contentArea">
				<?php echo $content; ?>
				<!-- End of Content -->
				<div class="clear"></div>
				
			</div>
			
			<!-- Copyright/legal text -->
			<div id="Copyright">
				<p>
					Copyright &copy; 2012 - cclub - All rights reserved. 
				</p>
			</div>
			
		</div>
</div>

<!-- Activate Font Replacement (cufón) -->
<script type="text/javascript"> Cufon.now(); </script>
<script type="text/javascript">
    Cufon.replace('-3.html');
    $("label.overlabel").overlabel();
    buttonStyles();
    roundCorners();
</script>
</body>

</html>

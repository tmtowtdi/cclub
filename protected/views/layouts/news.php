<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div class="two-thirds">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
    <div class="one-third">
		<div id="sidebar">
		<?php
            foreach(ChCategory::model()->allWithChannels() as $cat)
            {
			$this->beginWidget('zii.widgets.CPortlet', array(
				'title'=>$cat->name,
			));
			echo '<div class="sideNavWrapper"> <div class="sideNavBox-1"> <div class="sideNavBox-2">';
            $this->widget('zii.widgets.CMenu', array(
                'items'=>$cat->channelList(),
                'htmlOptions'=>array('class'=>'sideNav'),
                'activeCssClass'=>'currentPage',
            ));
            $this->endWidget();
            echo '</div> </div> </div>';
            }
		?>
		</div><!-- sidebar -->
	</div>
</div>
<?php $this->endContent(); ?>

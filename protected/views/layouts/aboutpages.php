<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div class="two-thirds">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
    <div class="one-third">
		<div id="sidebar">
		<?php
			$this->beginWidget('zii.widgets.CPortlet', array(
			));
			$this->widget('zii.widgets.CMenu', array(
				'items'=>$this->aboutMenu,
				'htmlOptions'=>array('class'=>'sideNav'),
			));
			$this->endWidget();
		?>
		</div><!-- sidebar -->
	</div>
</div>
<?php $this->endContent(); ?>

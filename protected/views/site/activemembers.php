<h1>Aktif Üyelerimiz</h1>
<p>Topluluk etkinliklerimizi, aktif üyelerimizin gönüllü katılımlarıyla gerçekleştirmekteyiz. Bu dönemki aktif üyelerimizin listesi aşağıdadır:</p>
<ul class="bullet-check">
<li>Alper Keskin (CENG-3)</li>
<li>Anton Semchonko (PHYS-2)</li>
<li>Atıl Koçkar (CENG-3)</li>
<li>Aysel Aksu (CEIT-3)</li>
<li>Ayşenur Durgut (CENG-4)</li>
<li>Bahadır Hatunoğlu (CENG-3)</li>
<li>Barış Michael Soylu (CENG-3)</li>
<li>Betül Altay (CENG-4)</li>
<li>Burak Bostancıoğlu (PHYS-2)</li>
<li>Doruk Duman (CENG-2)</li>
<li>Elif Özge Erdoğan (CENG-3)</li>
<li>Gökdeniz Karadağ (Ph. D)</li>
<li>İbrahim Sağıroğlu (CENG-2)</li>
<li>İlim Uğur (CENG-3)</li>
<li>Kader Belli (CENG-1)</li>
<li>Kamyar Ghasemlou (CENG-1)</li>
<li>Murat Akbal (CENG-3)</li>
<li>Murat Aydos (CENG-3)</li>
<li>Neslihan Doğan (CENG-3)</li>
<li>Ömer Demirer (CEIT-2)</li>
<li>Ömer Fatih Tanrıverdi (CENG-3)</li>
<li>Süleyman Şahin (CENG-3)</li>	
<li>Üstün Yıldırım<mm> (CENG-4)</li>
<li>Yiğit Yücel (ME-3)</li>

 
</ul>

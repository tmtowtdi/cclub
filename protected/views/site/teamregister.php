<div class="two-third">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'user-register-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

        <p>
        <?php echo $form->labelEx($model,'group_name',array('class'=>'overlabel')); ?>
        <?php echo $form->textField($model,'group_name',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'group_name'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'email',array('class'=>'overlabel')); ?>
        <?php echo $form->textField($model,'email',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'email'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'password',array('class'=>'overlabel')); ?>
        <?php echo $form->passwordField($model,'password',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'password'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'password_repeat',array('class'=>'overlabel')); ?>
        <?php echo $form->passwordField($model,'password_repeat',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'password_repeat'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'universite',array('class'=>'overlabel')); ?>
        <?php echo $form->textField($model,'universite',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'universite'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'katilimcilar',array('class'=>'overlabel')); ?>
        <?php echo $form->textArea($model,'katilimcilar',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'katilimcilar'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'tel',array('class'=>'overlabel')); ?>
        <?php echo $form->textField($model,'tel',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'tel'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'adres',array('class'=>'overlabel')); ?>
        <?php echo $form->textArea($model,'adres',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'adres'); ?>
        </p>

        <?php if(CCaptcha::checkRequirements()&& Yii::app()->user->isGuest):?>
        <p>
        <?php $this->widget('CCaptcha')?>
        </p>
        <p>
        <?php echo $form->labelEx($model, 'verifyCode',array('class'=>'overlabel'))?>
        <?php echo $form->textField($model, 'verifyCode',array('class'=>'textInput rounded'))?>
        <?php echo $form->error($model, 'verifyCode')?>
        </p>
        <?php endif?>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit',array('class'=>'btn')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>

<h1>Amaçlarımız</h1>
<ul>
<li>Akademik düzeyde eğitim alan öğrencilerin  bilgisayar bilimleri konusundaki sorularına yanıt vermek</li>
<li>Bilgisayar mantığını, teknolojisi, işlevi ve diğer bütün yönleriyle tanıtmak ve yaymak</li>
<li>Bilgisayarla ilgilenen ve ilgilenmek isteyenleri aynı çatı altında toplamak</li>
<li>Üniversite öğrencilerinin sürekli gelişen bilişim teknolojilerine ayak uydurmalarına yardımcı olmak</li>
<li>Bilişim kamuoyunun oluşması ve bu konudaki sivil toplum bilincinin şekillenmesi için yürütülen çalışmalara ODTÜ'de bulunmanın avantajlarını da kullanarak mümkün olan katkıyı sağlamak</li>
</ul>

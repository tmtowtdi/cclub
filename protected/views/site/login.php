<a href="#" class="loginClose" onclick="$.fn.fancybox.close(); return false;">Close</a>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
    'htmlOptions'=>array('class'=>'loginForm'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<div id="loginBg"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/icons/lock-and-key.png" width="128" height="128" alt="lock and key" /></div>
	<div class="loginContainer">
	<p class="note">Fields with <span class="required">*</span> are required.</p>

        
	<?php echo $form->errorSummary($model); ?>
		<fieldset>
			<legend>Account Login</legend>
			<p>
		<?php echo $form->labelEx($model,'email',array('class'=>'overlabel')); ?>
		<?php echo $form->textField($model,'email',array('class'=>'loginInput textInput rounded')); ?>
		<?php echo $form->error($model,'email'); ?>
			</p>
			<p>
		<?php echo $form->labelEx($model,'password',array('class'=>'overlabel')); ?>
		<?php echo $form->passwordField($model,'password',array('class'=>'loginInput textInput rounded')); ?>
		<?php echo $form->error($model,'password'); ?>
			</p>
            <p>
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
            </p>
		</fieldset>
	</div>
	<div class="hr" style="margin-bottom: 3px;"></div>
	<p class="right noMargin">

		<?php echo CHtml::submitButton('Login',array('class'=>'btn signInButton')); ?>

		<!--button type="button" class="btn" onclick="$.fn.fancybox.close(); return false;"><span>Cancel</span></button-->
	</p>
	<p class="left" style="margin: 14px 0 0 15px">
		<a href="#">Forgot your password?</a>
	</p>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	Cufon.replace('-3.html');
	$("label.overlabel").overlabel();
	buttonStyles();
	roundCorners();
</script>

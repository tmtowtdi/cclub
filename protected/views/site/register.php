<div class="two-third">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'user-register-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

        <p>
        <?php echo $form->labelEx($model,'email',array('class'=>'overlabel')); ?>
        <?php echo $form->textField($model,'email',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'email'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'password',array('class'=>'overlabel')); ?>
        <?php echo $form->passwordField($model,'password',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'password'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'password_repeat',array('class'=>'overlabel')); ?>
        <?php echo $form->passwordField($model,'password_repeat',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'password_repeat'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'name',array('class'=>'overlabel')); ?>
        <?php echo $form->textField($model,'name',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'name'); ?>
        </p>

        <p>
        <?php echo $form->labelEx($model,'lname',array('class'=>'overlabel')); ?>
        <?php echo $form->textField($model,'lname',array('class'=>'textInput rounded')); ?>
        <?php echo $form->error($model,'lname'); ?>
        </p>

        <?php if(CCaptcha::checkRequirements()&& Yii::app()->user->isGuest):?>
        <p>
        <?php $this->widget('CCaptcha')?>
        </p>
        <p>
        <?php echo $form->labelEx($model, 'verifyCode',array('class'=>'overlabel'))?>
        <?php echo $form->textField($model, 'verifyCode',array('class'=>'textInput rounded'))?>
        <?php echo $form->error($model, 'verifyCode')?>
        </p>
        <?php endif?>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit',array('class'=>'btn')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>

<h1>Biz Kimiz?</h1>
<p>ODTÜ Bilgisayar Topluluğu, ODTÜ Kültür İşleri Müdürlüğü denetiminde varlığını sürdüren bir öğrenci topluluğudur. Akademik danışmanı, ODTÜ Bilgisayar Mühendisliği Bölümü öğretim üyelerinden Prof. Dr. Göktürk Üçoluk'tur. 1989 yılında kurulmuş, yaptığı çalışmaları, geliştirdiği projeleri ve düzenlediği etkinlikleriyle her zaman ODTÜ'nün önde gelen öğrenci topluluklarından biri olmuş ve çalışmaları ülke genelinde örnek gösterilmiştir.</p>
</div> 

<h1>Yönetim Kurulumuz</h1>
<p>Topluluk etkinlik ve projelerinin koordinasyonu, her yıl genel kurulda seçilen yürütme kurulunun sorumluluğundadır. Topluluğumuzun 2010-2011 dönemi yürütme kurulu üyeleri aşağıdaki isimlerden oluşmaktadır:</p>
<ul class="bullet-check">
    <li>Süleyman Şahin (Eş Başkan)<br /><span class="contact">suleyman
    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span></li>
	<li>Burak Bostancıoğlu (Eş Başkan)<br /><span class="contact">bbostancioglu
    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span></li>
	<li>Alper Keskin<br /><span class="contact">akeskin
    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span></li>
  <li>Anton Semchenko<br /><span class="contact">anton
    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span></li>
	<li>İbrahim Sağıroğlu<br /><span class="contact">ibrahim
    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span></li>
  <li>Üstün Yıldırım<br /><span class="contact">ustun
    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span></li>
	<li>Yiğit Yücel<br /><span class="contact">yyucel
    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span></li>
</ul>

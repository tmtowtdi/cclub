<?php
$this->pageTitle=Yii::app()->name . ' - Contact Us';
?>
<h1> Bize ulaşın: </h1>
<p>ODTÜ Bilgisayar Mühendisliği Bölümü B Blok Zemin Kat 10 Numaralı odadır (BZ-10). Bizle tanışmak, bir şeyler sormak, yardım istemek, bir işin ucundan tutmak ya da geçerken uğrayıp sohbet etmek isteyen herkesi odamıza bekleriz.</p>
<p>Odamız, üyelerimizin boş vakti olduğu sürece açıktır. Bize daha rahat ulaşabilmek için, en çok kişinin odada olduğu saatler olan 16.00-18.00 saatlerinde gelmenizi tavsiye ederiz.</p>
<p>Bunun dışında 0 312 210 55 30 numaralı telefondan (ODTÜ İçi 5530), ya da <span class="contact">hot-line<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/contact.png'); ?></span> adresinden de bize ulaşabilirsiniz.</p>

<h2> Ya da Daha Hızlı Ulaşın ! </h2>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>

</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name', array('class' => 'overlabel')); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email', array('class' => 'overlabel')); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject', array('class' => 'overlabel')); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body', array('class' => 'overlabel')); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<div>
		<?php $this->widget('CCaptcha'); ?>
		</div>
		<div>
		<?php echo $form->labelEx($model,'verifyCode', array('class' => 'overlabel')); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>

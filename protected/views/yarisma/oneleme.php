<p>
    Yarışmaya katılmak için <?php
        echo CHtml::link('giriş', array('login'));
    ?> yapın
</p>
<h1>Ön Eleme Soruları</h1>

<p>Bu seneki yarışma sorularına çözüm gönderebilmek için üye olunuz.</p>

<p>Üye olduktan sonra her sorunun altında bir input dosyası indirebilirsiniz. İndirdiğiniz dosyaya ait sonucu yüklemek için 15 dakika zamanınız olacak.
Bu süre içinde doğru cevabı yüklemezseniz bu soru için şansınızı kaybedeceksiniz. Bu yüzden yükleme isteği yapmadan önce programınızın doğru çalıştığından emin olun. </p>

<p>Yarışmamıza katılmak için öncelikle Başvuru Formu sayfasındaki Kayıt Formunu kullanarak bir kullanıcı adı ve parola almanız gerekmekte. 
	Sonradan sorularınızı çözdükçe çözüm gönderme bağlantısını kullanarak dosyalarınızı bize gönderebilirsiniz. 
	Ön eleme sorularının ve özel sorunun çözümleri için son gönderim zamanı 14 Mayıs 2012, 23:59'dur.</p>

<p>Finalistler belirlenmeden önce önelemede yüklenen kaynak kodlarının deneneceği bilgisayarın <br />

    <b>İşlemci bilgileri:</b><br />

  Intel Core(TM)2 Duo CPU  E4600  @ 2.40GHz<br />

   <b>Hafıza bilgileri:</b><br />

     4GB RAM<br/>

     <b>İşletim Sistemi</b><br />

     Ubuntu 10.10 - GNU/Linux</p>




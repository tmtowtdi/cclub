<h1>Yarışma Ortamı</h1>
<p>Tüm finalist gruplardan her üyeye eşit özelliklere sahip ve Ubuntu Linux kurulu bilgisayarlar ODTÜ Bilgisayar Topluluğu tarafından sağlanacaktır. Sağlanacak diğer araçlar ve programlar aşağıda yer almaktadır.</p>
<h2>GELİŞTİRME ARAÇLARI</h2>
<p>Final anında yarışmacılara aşağıdaki geliştirme araçları sağlanacaktır:</p>
<p><b>GNU libc(glibc)</b><br />
ISO C standardının öngördüğü C fonksiyonlarına ek olarak POSIX, SVID ve BSD tarafından tanımlanmış birçok fonksiyonu içerir. Kurulu olanderleyiciler, varsayılan olarak glibc ile dinamik bağlanmış çalıştırılabilirler üretecektir.</p>
<p><b>GNU C(gcc)</b><br />
ISO/ANSI C standardına ek olarak bazı genişletmeler de sunan bir C derleyicisi.</p>
<p><b>GNU C++(g++)</b><br />
ISO/ANSI C++ standardına uygun bir C++ derleyicisi. GNU C gibi GNU C++ da bazı ek fonksiyonlar sunmakta.</p>
<p><b>JAVAC</b><br />
Java derleyicisi.</p>
<p><b>GNU Debugger(gdb)</b><br />
Komut satırı tabanlı kod ayıklayıcı. xxgdb ve kdbg gibi sunulan grafik arayüzlü kod ayıklayıcılar gdb'yi içsel olarak kullanırlar.</p>
<p>Yarışmada bu programlar tarafından sağlanan ve standart olmayan genişletmeleri kullanmak serbesttir. Ancak bu genişletmeler bir avantaj sağlamayacaktır. Bu geliştirme araçlarının belgeleri, info ile okunabilecektir.</p>
<h2>EDİTÖRLER</h2>
<p>Yarışmacılar, programlarını sağlanan editörlerin herhangi birini kullanarak yazmakta serbesttirler.</p>
<p>Aşağıdaki editörler, kullanıcılara sunulan editörler arasındadır:</p>
<p><b>vim</b><br />
vi editörü temel alınarak geliştirilmiş bir editör.</p>
<p><b>pico</b><br />
Temel editör işlevleri sunan kolay kullanımlı bir editör.</p>
<p><b>kedit/kwrite/kate/gedit</b><br />
KDE ve Gnome masaüstü ortamlarıyla gelen, kolay kullanımlı editörler.</p>
<p><b>emacs</b><br />
Oldukça güçlü, özel işlevler sunan bir editör.</p>
<p>Bütün bunlara ek olarak, Turbo C'nin IDE'sine çok benzer bir arayüz sunan rhide yarışmacılara sağlanacaktır.</p>

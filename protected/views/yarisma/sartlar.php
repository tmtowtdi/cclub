<h1>Katılma Şartları</h1>

<p>Yarışmaya, T.C. veya K.K.T.C. sınırları içindeki bir eğitim kurumunda, <b>üniversite</b> lisans, önlisans, yüksek lisans veya dengi eğitim programlarında öğrenim gören öğrencilerden oluşan 2 ya da 3 kişilik gruplar katılabilir.</p>

<p>Bilgisayar ve matematikle ilgili iseniz, zor problemleri verimli bir şekilde çözmekten hoşlanıyorsanız, 24 Nisan - 15 Mayıs tarihleri arasında ön eleme sorularını çözün ve başvuru formunu doldurup yazdığınız programları bize yollayın.</p>

<p>Yarışmacı adaylarının ön eleme soruları için yazdıkları programları <b>en geç 15 Mayıs 2012 akşamı 23:59'a kadar</b> web sayfamız aracılığı ile göndermeleri gerekmektedir. Ön elemeyi geçmeyi başaranlar finale katılmaya hak kazanacak, yarışma finali <b>19 Mayıs 2012</b> tarihinde ODTÜ Bilgisayar Mühendisliği Bölümü'nde gerçekleşecektir.</p>




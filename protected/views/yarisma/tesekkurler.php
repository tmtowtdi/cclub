<h1>Teşekkürler</h1>
<p>Bu yarışmayı hazırlamamız sırasında her türlü destekleri ile yanımızda olan,</p>
<ul>
<li>Akademik danışmanımız Prof.Dr.Göktürk Üçoluk'a</li>
<li>ODTÜ Bilgisayar Mühendisliği Bölümü'ne</li>
<li>ODTÜ Kültür İşleri Müdürlüğü'ne</li>
<li>Soruları hazırlayıp değerlendiren teknik ekibimize</li>
<li>Bize destek olan tüm sponsorlarımıza</li>
<li>Organizasyonun gerçekleşmesi için çalışan tüm üyelerimize</li>
</ul>
<p>teşekkürü borç biliriz.</p>


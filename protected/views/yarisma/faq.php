<h1>Sıkca Sorulan Sorular</h1>
<p><b>SORU:</b>
Programlarımızı upload ederken nasıl isimlendireceğiz? Her soru için kesin bir isim var mı?</p>
<p><b>CEVAP:</b>
İsimlendirme önemli değil ama uzantınızın .c .cpp veya .java olması gerekmektedir.</p>
<br />
<p><b>SORU:</b>
Eleme sorularının değerlendirileceği bilgisayarın özelliklerini ve varsa süre/hafıza sınırlamalarını da belirtir misiniz?</p>
<p><b>CEVAP:</b> Yarışma ana sayfasının "Ön Eleme Soruları" linkinden ulaşabilirsiniz.</p>
<br />
<p><b>SORU:</b>
Grubun 2 ya da 3 kişi olmasının bir farkı var mı?</p>
<p><b>CEVAP:</b>
Grubunuzu ister 2, ister 3 kişi oluşturmakta serbestsiniz. Ancak final aşamasında her yarışmacıya birer bilgisayar verileceğinden, soruları uygun parçalara bölmeniz halinde 3 kişi katılmanız daha avantajlı olabilir.</p>
<br />
<p><b>SORU:</b> Grup arkadaşı bulamazsam tek kişi katılabilir miyim?</p>
<p><b>CEVAP:</b> Yarışmamıza katılmak için en az 2 kişilik bir grubunuz olmalıdır.</p> 
<br />
<p><b>SORU:</b>
Ödüller kişilere mi gruplara mı verilecek?</p>
<p><b>CEVAP:</b>
<?php
    echo CHtml::link('Ödüller sayfası', array('oduller'));
?>nda belirtilen ödüllerin her biri, o dereceyi alan gruplara verilecek toplam miktardır. Grup üyeleri, bu ödülleri istedikleri gibi paylaşmakta serbesttir.</p>

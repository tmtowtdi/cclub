<?php 
	$this->setPageTitle('15. Geleneksel Programlama Yarışması');	
?>
<h1>XV. Geleneksel Programlama Yarışması</h1>

<p>ODTÜ Bilgisayar Topluluğu, üniversite öğrencilerini bilgisayar bilimlerine teşvik etmek amacıyla, her sene geleneksel olarak üniversite öğrencileri arası programlama yarışmasını düzenlemektedir.</p>

<p>Bu sene 15.sini gerçekleştirdiğimiz yarışmamıza katılmak için tek yapmanız gereken, 2 veya 3 kişilik <b>grubunuzu</b> kurduktan sonra, ön eleme sorularını çözerek 14 Mayıs 2012 günü saat 23.59'a kadar sitemizden yüklemek. Çözüm yolunu bulduktan sonra yazacağınız C, C++ veya Java dillerindeki programınızı sınayarak sitemize yükleyebilirsiniz. Üstelik başvuruda bulunmak için tüm sorulara cevap göndermek zorunda değilsiniz.</p>

<p>Gönderdiğiniz programlar, Linux işletim sistemi kurulu olan bir bilgisayarda derlenerek değerlendirilecek ve belirlenen 10-12 <b>finalist grup</b>, 19 Mayıs 2012 günü ODTÜ Bilgisayar Mühendisliği Bölümü bilgisayar laboratuvarlarında yapılacak finale katılacaklardır. Ankara dışından finale katılmaya hak kazanan yarışmacıların ODTÜ'de konaklamaları ve yol masrafları<b>*</b> topluluğumuz tarafından karşılanacaktır.</p>

<p>Final aşamasında yine benzer formatta sorularla karşılaşacak gruplar bu kez <b>zamana karşı</b> yarışacaklardır. Final aşamasında gruplardaki her yarışmacıya birer bilgisayar verilecek, ancak grup olmanın avantajını kullanmaları beklenecektir.</p>

<p>20 Mayıs 2012 Pazar günü ise ODTÜ Kültür Kongre Merkezi'nde etkinlik kapsamında, yazılım sektörünün büyük şirketlerinin katıldığı seminerler düzenlenecektir. Aynı gün yapılacak olan <b>ödül töreni</b>nde tüm finalistlere sürpriz hediyeler verileceği gibi, ilk 3 dereceye giren gruplara 
<?php
    echo CHtml::link('ödüller', array('oduller'));
?>
verilecektir.</p>

<br />

<p><b>*</b> Sadece yurtiçinden otobüs ve trenle gelecek olan finalistler için geçerlidir.</p>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
<?php
    echo CHtml::link('Kayıt olmak için tıklayınız', array('site/teamRegister'));
?>

        
	<?php echo $form->errorSummary($model); ?>
		<fieldset>
			<legend>Account Login</legend>
			<p>
		<?php echo $form->labelEx($model,'email',array('class'=>'overlabel')); ?>
		<?php echo $form->textField($model,'email',array('class'=>'loginInput textInput rounded')); ?>
		<?php echo $form->error($model,'email'); ?>
			</p>
			<p>
		<?php echo $form->labelEx($model,'password',array('class'=>'overlabel')); ?>
		<?php echo $form->passwordField($model,'password',array('class'=>'loginInput textInput rounded')); ?>
		<?php echo $form->error($model,'password'); ?>
			</p>
            <p>
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
            </p>
		</fieldset>
	<div class="hr" style="margin-bottom: 3px;"></div>
	<p class="right noMargin">
		<?php echo CHtml::submitButton('Login',array('class'=>'btn signInButton')); ?>
	</p>
<?php $this->endWidget(); ?>

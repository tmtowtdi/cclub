<?php
class PostsWidget extends CWidget
{
    public $dataProvider;

    public function run()
    {
        $this->widget('zii.widgets.CListView',array(
            'dataProvider'=>$this->dataProvider,
            'itemView' => 'postView',
            'template' => '<ul>{items}</ul> {sorter} {pager}',
            'summaryText' => '',
            'emptyText' => '',
        ));
    }
}

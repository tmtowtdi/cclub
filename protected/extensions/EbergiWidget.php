<?php
class EbergiWidget extends CWidget
{
    public $rssLink = "http://e-bergi.com/rss.xml";
    public $htmlOptions = array('class' => 'bullet-disc-red');

    public function run()
    {
        echo $this->getItems();
    }

    public function getItems()
    {
        require_once("simple_html_dom.php");
        $out = CHtml::openTag("ul",$this->htmlOptions);
        $html = file_get_html($this->rssLink);
        foreach($html->find("item") as $item)
        {
            $title = $item->find('title',0)->innertext;
            $link = $item->find('guid',0)->innertext;
            $out .= CHtml::tag('li',array(),CHtml::link($title,$link));
        }
        $out .= CHtml::closeTag("ul");
        return $out;
    }
}

<?php
class CommentsWidget extends CWidget
{
    public $dataProvider;

    public function run()
    {
        $this->widget('zii.widgets.CListView',array(
            'dataProvider'=>$this->dataProvider,
            'itemView' => 'commentView',
            'template' => '<ul id="Comments">{items}</ul> {sorter} {pager}',
            'summaryText' => '',
            'emptyText' => '',
        ));
    }
}

<?php
class PostInputWidget extends CWidget
{
    public $profileId;
    public $channelId;
    public function run()
    {
        $model = new Post;
        $model->profile_id = $this->profileId;
        $model->channel_id = $this->channelId;
        Yii::app()->clientScript->registerCss('postbox','#PostBox textarea{width: 400px}');
        $this->render('postForm',array(
            'model' => $model,
        ));
    }
}

<?php
class Etkinlikler extends CWidget
{
    public $dataProvider;

    public function run()
    {
        $this->widget('zii.widgets.CListView',array(
            'dataProvider'=>$this->dataProvider,
            'itemView' => 'etkinlikView',
            'template' => '<div class="featuredContent">{items}</div>',
            'summaryText' => '',
            'emptyText' => '',
        ));
    }
}

<?php
class CommentInputWidget extends CWidget
{
    public $postId;
    public $photoId;
    public function run()
    {
        $model = new Comment;
        if(isset($this->postId))
            $model->post_id= $this->postId;
        else if(isset($this->photoId))
            $model->photo_id = $this->photoId;
        else
        {
            die();
        }
        $this->render('commentForm',array(
            'model' => $model,
        ));
    }
}

<!-- Comment -->
<div class="commentBox">
    <p style="margin:0;" class="commentButton"><?php echo CHtml::link('Comment',''); ?></p>
    <div class="commentInput">
    <?php echo CHtml::beginForm(array('/comment/create'),'post',array('id'=>'PostForm'));?>
    <?php echo CHtml::activeHiddenField($model,'post_id') ?>
    <?php echo CHtml::activeHiddenField($model,'photo_id') ?>
    <?php echo CHtml::activeTextArea($model,'content',array('rows'=>3,'style' => 'width:420px;')) ?>
    <p style="margin:0;float:right"><?php echo CHtml::submitButton('Submit'); ?></p>
    <?php echo CHtml::endForm(); ?>
    </div>
</div>

<!-- Post -->
<div id="PostBox">
    <?php echo CHtml::beginForm(array('/post/create'),'post',array('id'=>'PostForm'));?>
    <?php    if(isset($this->profileId)) {echo CHtml::activeHiddenField($model,'profile_id');  }
             else if(isset($this->channelId)){ echo CHtml::activeHiddenField($model,'channel_id');} ?>
    <div><?php echo CHtml::activeLabel($model,'content',array('for'=>'PostInput','class' => 'overlabel')) ?>
    <?php echo CHtml::activeTextArea($model,'content',array('id'=>'PostInput','rows'=>6)) ?></div>
    <p style="margin:0;"><?php echo CHtml::submitButton('Submit',array('id'=> 'PostSubmit')); ?></p>
    <?php echo CHtml::endForm(); ?>
</div>

<li class="view">
<div class="quote">
    <div class="quoteBox-1">
        <div class="quoteBox-2">
            <p><?php echo $data->shortenedText(); ?> </p>
        </div>
    </div>
</div>
<div class="quoteAuthor">
    <p class="name"><?php echo CHtml::encode($data->creator->name); ?></p>
    <p class="details"><?php echo CHtml::encode(($data->creationTime)); ?></p>
</div>
</li>
<?php if($data->commentCount): ?>
<?php echo CHtml::link("Click to see $data->commentCount comment",array('/post/view','id' => $data->id));?>
<?php else: ?>
<p> There is no comment for this post be the first :D </p>
<?php endif; ?>
<?php $this->widget('ext.CommentInputWidget',array(
    'postId' => $data->id,
)); ?>
<div class="hr"></div>

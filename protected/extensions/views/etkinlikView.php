<div class="featuredItem">
    <a href="<?php echo $data->posterLink;?>" class="featuredImg img" rel="featured">
        <?php echo CHtml::image($data->getPosterLink());?>
    </a>
    <div class="featuredText">
        <h1 class="title">
            <?php echo $data->title;?>
            <span><?php echo $data->short;?></span>
        </h1>
        <?php echo CHtml::link('Ayrıntılar',array('/activity/view','id'=>$data->id));?>
    </div>
</div>


<?php

class YarismaController extends Controller
{
    public $layout='//layouts/yarismapages';
    public $isLogin = false;
    protected $yarismaMenu = array(
        array('label'=>'Genel Bilgi', 'url'=>array('yarisma/genelBilgi')),
        array('label'=>'2012 Takvimi', 'url'=>array('yarisma/takvim')),
        array('label'=>'Ödüller', 'url'=>array('yarisma/oduller')),
        array('label'=>'Yarışma Hakkında', 'url'=>array('yarisma/oneleme')),
        array('label'=>'Katılma Şartları', 'url'=>array('yarisma/sartlar')),
        array('label'=>'Yarışma Ortamı', 'url'=>array('yarisma/ortam')),
        array('label'=>'Sponsorlar', 'url'=>array('yarisma/sponsorlar')),
        array('label'=>'Teknik Ekip', 'url'=>array('yarisma/ekip')),
        array('label'=>'Sıkça Sorulan Sorular', 'url'=>array('yarisma/faq')),
        array('label'=>'Teşekkürler', 'url'=>array('yarisma/tesekkurler')),
        array('label'=>'İletişim', 'url'=>array('site/contact')),
    );

	public function actionIndex()
	{
		$this->redirect(array('genelbilgi'));
	}

	/**
	 * Lists all models.
	 */
	public function actionFinalists()
	{
		$this->render('final');
	}

	public function actionGenelBilgi()
	{
		$this->render('genelbilgi');
	}

	public function actionTakvim()
	{
		$this->render('takvim');
	}

	public function actionOduller()
	{
		$this->render('oduller');
	}

	public function actionOneleme()
	{
		if(!Yii::app()->team->isGuest)
		{
			$this->redirect(array('/oneleme/index'));
		}
		$this->render('oneleme');
	}

	public function actionSartlar()
	{
		$this->render('sartlar');
	}

	public function actionOrtam()
	{
		$this->render('ortam');
	}

	public function actionSponsorlar()
	{
		$this->render('sponsorlar');
	}

	public function actionEkip()
	{
		$this->render('ekip');
	}

	public function actionFaq()
	{
		$this->render('faq');
	}

	public function actionTesekkurler()
	{
		$this->render('tesekkurler');
	}

	public function actionLogout()
	{
		Yii::app()->team->logout();
		$this->redirect(array('/yarisma'));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new TeamLoginForm;
        $this->isLogin = true;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='teamLogin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data
		if(isset($_POST['TeamLoginForm']))
		{
			$model->attributes=$_POST['TeamLoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form

        $this->render('login',array('model'=>$model));
	}
}

<?php

class ActivityController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
                'roles'=>array('guest'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        if(!isset($model))
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        $media = new CActiveDataProvider('media',array(
            'criteria' => array(
                'condition'=>'activity_id=:id',
                'params'=>array(':id'=>$model->id),
            ),
        ));
        if(Yii::app()->user->checkAccess('YK') && isset($_POST['Media']))
		{
            $mmedia = new Media;
            $mmedia->attributes=$_POST['Media'];
            $mmedia->file = CUploadedFile::getInstance($mmedia,'file');
            if($mmedia->save())
            {
                $fpath = getcwd() . DIRECTORY_SEPARATOR . "files" ;
                if(!file_exists($fpath)) mkdir($fpath);
                $path = $fpath. DIRECTORY_SEPARATOR. $mmedia->id.".".$mmedia->file->getExtensionName();

                $mmedia->file->saveAs($path);
                $mmedia->file = $mmedia->id . ".".$mmedia->file->getExtensionName();
                $mmedia->save(false);
            }
		}
		$this->render('view',array(
			'model'=>$model,
            'media' => $media,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Activity;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Activity']))
		{
			$model->attributes=$_POST['Activity'];
            $model->date = time($_POST['Activity']['date']);
            $model->poster = CUploadedFile::getInstance($model,'poster');
            if($model->save())
            {
                print_r( $model);
                $model->poster = CUploadedFile::getInstance($model,'poster');
                if(!file_exists($model->fpath)) mkdir($model->fpath);
                $name = $model->fpath.$model->id.".".$model->poster->getExtensionName();
                $model->poster->saveAs($name);
                $afis = Yii::app()->image->load($name);
                $afis->resize(273,0,Image::WIDTH);
                $afis->save();
                $model->poster = $model->id.".".$model->poster->getExtensionName();
                if($model->save(false))
                    $this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Activity']))
		{
			$model->attributes=$_POST['Activity'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Activity');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Activity('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Activity']))
			$model->attributes=$_GET['Activity'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Activity::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='activity-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

class OnelemeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array (
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xE1E2E3,
			),
		);
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function filterAccessControl($filterChain)
	{
	    $filter=new TeamAccessControlFilter;
	    $filter->setRules($this->accessRules());
	    $filter->filter($filterChain);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'captcha'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create', 'show', 'RequestInput', 'update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionRequestInput($id)
	{
		$passed = false;
		$model = $this->loadModel($id);
		$rmodel = QuestionResult::model()->findByAttributes(array(
					'team_id' => Yii::app()->team->id,
					'question_id' => $id,
				));
		if(!$rmodel)
		{
			$iomodel = QuestionIo::model()->find(array
				(
					'select' =>'*, rand() as rand',
					'condition' => 'question_id = :qi',
					'limit' => '1',
					'order' => 'rand',
					'params' => array(':qi' => $id),
				));

			$rmodel = new QuestionResult;
			$rmodel->team_id = Yii::app()->team->id;
			$rmodel->question_id = $id;
			$rmodel->input_id = $iomodel->id;
			$rmodel->result = 0;
			$rmodel->request_time = time() + 5 * 60;
			if($rmodel->save())
			{
				$passed = true;
			}
		}
		else
		{
			$passed = true;
			$iomodel = QuestionIo::model()->findByPk($rmodel-> input_id);
		}

		if(!$iomodel)
			throw new CHttpException(404,'The requested page does not exist.');

		if($passed)
		{
			header('Content-Disposition: attachment; filename="input.' . $model->name . '.txt"');	
			header('Content-type: text/plain');
			$fpath = getcwd() . DIRECTORY_SEPARATOR . "inputs" . DIRECTORY_SEPARATOR . 'input-' . $iomodel->id . '.txt';
			if(file_exists($fpath))
				echo file_get_contents($fpath);
			else
				echo $iomodel->input;
			die();
		}
		else
			throw new CHttpException(404,'The requested page does not exist.');

	}

	public function actionShow($id)
	{
        $umodel = new QuestionUploadForm;
		$model = $this->loadModel($id);
		$qmodel = QuestionResult::model()->findByAttributes(array('team_id' => Yii::app()->team->id, 'question_id' => $id));
		$timePassed = $qmodel ? (time() - $qmodel->request_time > 5 * 60) : false;

        if(isset($_POST['QuestionUploadForm']))
		{
            $umodel->attributes=$_POST['QuestionUploadForm'];
            $umodel->outputFile = CUploadedFile::getInstance($umodel,'outputFile');
            $umodel->sourceFile = CUploadedFile::getInstance($umodel,'sourceFile');
            if($umodel->validate())
            {
            	if(!$qmodel || $timePassed)
					throw new CHttpException(404,'The requested page does not exist.');


				if($qmodel->result < 100)
				{
	                $fpath = getcwd() . DIRECTORY_SEPARATOR . "solutions" . DIRECTORY_SEPARATOR . Yii::app()->team->id;
	                if(!file_exists($fpath)) mkdir($fpath);
	                $fpath = $fpath . DIRECTORY_SEPARATOR. $umodel->questionId ;
	                if(!file_exists($fpath)) mkdir($fpath);
	                $fpath = $fpath . DIRECTORY_SEPARATOR;

	                $outputFileName = $fpath . 'output-'.$qmodel->input_id.'.txt';
	                $umodel->outputFile->saveAs($outputFileName);
					$iomodel = QuestionIo::model()->findByAttributes(array('id' => $qmodel->input_id));

					$opath = getcwd() . DIRECTORY_SEPARATOR . "outputs" . DIRECTORY_SEPARATOR . 'output-' . $iomodel->id . '.txt';
					
					if(file_exists($opath))
						$iomodel->output = file_get_contents($opath);

	                $output = file_get_contents($outputFileName);
	                
	                $toreplace = array('\n','\r','\t',' ');
	                $output = str_replace($toreplace, '', $output);
	                $iomodel->output = str_replace($toreplace, '', $iomodel->output);
	                
	                if(md5($output) == md5($iomodel->output))
	                	$qmodel->result = 100;

	                $qmodel->save();

	                $umodel->sourceFile->saveAs($fpath . 'source-'.$qmodel->question_id.'.'. $umodel->sourceFile->getExtensionName());
				}
            }
		}

		$this->render('show',array(
			'model'=>$model,
			'qmodel'=> $qmodel,
			'umodel' => $umodel,
			'timePassed' => $timePassed,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new YarismaQuestion;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['YarismaQuestion']))
		{
			$model->attributes=$_POST['YarismaQuestion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['YarismaQuestion']))
		{
			$model->attributes=$_POST['YarismaQuestion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider= YarismaQuestion::model()->findAll();
		$results = QuestionResult::model()->findAll(array('index'=>'question_id','condition' => 'team_id = :ti', 'params' => array(':ti' => Yii::app()->team->id)));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'results' => $results,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new YarismaQuestion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['YarismaQuestion']))
			$model->attributes=$_GET['YarismaQuestion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=YarismaQuestion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='yarisma-question-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

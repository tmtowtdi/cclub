<?php

class SiteController extends Controller
{
    protected $aboutMenu = array(
        array('label'=>'Biz kimiz?', 'url'=>array('site/bizkimiz')),
        array('label'=>'Amaçlarımız', 'url'=>array('site/amaclar')),
        array('label'=>'Yönetim Kurulumuz', 'url'=>array('site/yonetim_kurulu')),
        array('label'=>'Aktif Üyelerimiz', 'url'=>array('site/aktif_uyeler')),
        array('label'=>'Bize Katılın', 'url'=>array('site/bize_katilin')),
    );
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array (
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xE1E2E3,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
        $etkinlikler = new CActiveDataProvider('Activity',array(
            'criteria' => array(
                'limit' => 4,
                'order' => 'date',
            ),
        ));
        $this->render('index',array('etkinlikler' => $etkinlikler));
	}

	public function actionAmaclar()
	{
        $this->layout='//layouts/aboutpages';
        $this->menu = $this->aboutMenu;
        $this->render('amaclar');
	}

	public function actionBizkimiz()
	{
        $this->layout='//layouts/aboutpages';
        $this->render('bizkimiz');
	}

	public function actionYonetim_kurulu()
	{
        $this->layout='//layouts/aboutpages';
        $this->render('yk');
	}

    public function actionAktif_uyeler()
	{
        $this->layout='//layouts/aboutpages';
        $this->render('activemembers');
	}

    public function actionBize_katilin()
	{
        $this->layout='//layouts/aboutpages';
        $this->render('joinus');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail('tmtowtdio@gmail.com',$model->subject,$model->body,$headers);
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form

        if(Yii::app()->request->isAjaxRequest)
            $this->renderPartial('login',array('model'=>$model));
        else
            $this->render('login',array('model'=>$model));
	}

    public function actionTeamRegister()
    {
		$model=new TeamRegisterForm;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TeamRegisterForm']))
		{
			$model->attributes=$_POST['TeamRegisterForm'];
			if($model->validate())
            {
                $team = new YarismaTeams;
                $team->attributes = $_POST['TeamRegisterForm'];
                if($team->save())
    				$this->redirect(array('/yarisma'));
                else
                    $model->addErrors($team->errors);
            }
		}

        $this->render('teamregister', array(
                'model'=>$model,
            ));
    }

    public function actionRegister()
    {
		$model=new RegisterForm;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RegisterForm']))
		{
			$model->attributes=$_POST['RegisterForm'];
			if($model->validate())
            {
                $user = new User;
                $user->attributes = $_POST['RegisterForm'];
                if($user->save())
    				$this->redirect(array('user/view','id'=>$user->id));
                else
                    $model->addErrors($user->errors);
            }
		}

		$this->render('register',array(
			'model'=>$model,
		));
    }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}

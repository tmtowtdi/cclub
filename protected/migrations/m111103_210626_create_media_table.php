<?php

class m111103_210626_create_media_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('cclub_media',array(
            'id' => 'pk',
            'activity_id' => 'int(11) NOT NULL',
            'text' => 'string',
        ));
    }

	public function down()
	{
		echo "m111103_210626_create_media_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}

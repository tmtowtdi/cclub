<?php

class m120214_185425_create_yarisma_teams extends CDbMigration
{
    public function up()
    {
        $this->createTable('yarisma_teams',array(
            'id' => 'pk',
            'email' => 'string NOT NULL',
            'password' => 'string NOT NULL',
            'universite' => 'string NOT NULL',
            'katilimcilar' => 'string NOT NULL',
            'tel' => 'string NOT NULL',
            'adres' => 'string NOT NULL',
        ));
    }

    public function down()
    {
        echo "crate_users_table does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
     */
}

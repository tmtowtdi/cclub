<?php

class m120214_204435_add_team_name extends CDbMigration
{
	public function up()
	{
        $this->addColumn('yarisma_teams','group_name','string NOT NULL');
	}

	public function down()
	{
		echo "m120214_204435_add_team_name does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}

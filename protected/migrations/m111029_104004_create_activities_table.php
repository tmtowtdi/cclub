<?php
class m111029_104004_create_activities_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('cclub_activities',array(
            'id' => 'pk',
            'title' => 'varchar(255) NOT NULL',
            'date' => 'int(11) NOT NULL',
            'short' => 'string NOT NULL',
            'content' => 'string NOT NULL',
            'poster' => 'string NOT NULL',
        ));
	}

	public function down()
	{
		echo "m111029_104004_create_activities_table does not support migration down.\n";
        return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}

<?php

class m110930_202918_create_users_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('cclub_users',array(
            'id' => 'pk',
            'email' => 'string NOT NULL',
            'password' => 'string NOT NULL',
            'name' => 'string NOT NULL',
            'lname' => 'string NOT NULL',
            'role' => 'int(2) NOT NULL',
        ));
	}

	public function down()
	{
		echo "crate_users_table does not support migration down.\n";
        return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}

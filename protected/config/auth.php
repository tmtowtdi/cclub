<?php
return array(
    'guest' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Guest',
        'bizRule' => null,
        'data' => null
    ),
    'YeniUye' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'YeniUye',
        'children' => array(
            'guest',
        ),
        'bizRule' => null,
        'data' => null
    ),
    'ResmiUye' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'ResmiUye',
        'children' => array(
            'YeniUye', 
        ),
        'bizRule' => null,
        'data' => null
    ),
    'CgSorumlusu' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'CgSorumlusu',
        'children' => array(
            'ResmiUye',          
        ),
        'bizRule' => null,
        'data' => null
    ),
    'YK' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'YK',
        'children' => array(
            'CgSorumlusu',          
        ),
        'bizRule' => null,
        'data' => null
    ),
    'Admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            'YK',         
        ),
        'bizRule' => null,
        'data' => null
    ),
);
